package module3;

import java.util.Hashtable;

public class HashsetDemo {
    public static void main(String[] args) {
        Hashtable<String, Integer> hashtable = new Hashtable<>();
        hashtable.put("age", 30);
        hashtable.put("children", 5);
        hashtable.put("income", 100000);
        // this is the advantage of having key in hashtable - easy to refer to any data
        System.out.println("Income = " + hashtable.get("income"));
    }
}
