package module3;
import java.util.*; // import all classes inside java.util package

public class ListDemo {
    public static void main(String[] args) {
        // <String> = Generic
        ArrayList<String> list = new ArrayList<String>();
        list.add("Azman"); // index 0
        list.add("Ravi"); // index 1
        list.add("Ah Chong");
        list.add("John Doe");
        System.out.println("Name = " + list.get(1));
        // travesing list through Iterator
        Iterator itr = list.iterator(); // loop object
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }

        // this technique is not advisable to use
        ArrayList jumble = new ArrayList();
        jumble.add("Testing");
        jumble.add(10);
    }
}
