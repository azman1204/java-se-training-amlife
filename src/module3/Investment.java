package module3;

import java.util.Hashtable;
import java.util.Iterator;

public class Investment {
    public static void main(String[] args) {
        // int vs Integer - every primitive type, have pair reference type
        // short vs Short, long vs Long, double vs Double
        // Hashtable accept only non-primitive type
        Hashtable<String, Integer> list = new Hashtable<>();
        list.put("azman", 500_000);
        list.put("Jason", 600_000);
        list.put("Ravi", 700_000);
        list.put("CK", 800_000);
        list.put("Hafiz", 900_000);
        list.put("Sandy", 1_000_000);
        Iterator itr = list.keySet().iterator(); // return a list of keys (azman, ravi, ...)
        while (itr.hasNext()) {
            String key = itr.next().toString();
            int investment = list.get(key);
            //System.out.println("Key = " + key + " value = " + investment);
            for(int year=1; year <= 10; year++) {
                int profit = (int)(investment * (0.06));
                investment = investment + profit;
            }
            System.out.println(key + " " + investment);
        }
    }
}
