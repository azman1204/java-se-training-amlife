package module3;

public class ArrayDemo {
    public static void main(String[] args) {
        ArrayDemo demo = new ArrayDemo();
        demo.start();
    }

    public void start() {
        // array is fixed in length
        // how to create an array of integer - method 1
        int[] ages = new int[3];
        ages[0] = 10;
        ages[1] = 15;
        ages[2] = 20;
        ages[3] = 25; // this should error
        // method 2
        String[] names = {"john", "jane", "doe"};
    }
}
