package module8;

public class GenericList<T> {
    private T[] items = (T[]) new Object[10];
    private int count;

    private void add(T item) {
        items[count++] = item;
    }

    public static void main(String[] args) {
        var list = new GenericList<Integer>();
        list.add(100);
        list.add(200);
        //list.add("string.."); // error
        var list2 = new GenericList<String>();
        list2.add("Hello");
    }
}
