package module8;

public class ListDemo {
    private int[] items = new int[10];
    private int count = 0;

    public static void main(String[] args) {
        var demo = new ListDemo();
        demo.add(100);
        demo.add(200);
    }

    public void add(int item) {
        items[count++] = item;
    }
}

class ListDemo2 {
    private Object[] items = new Object[10];
    private int count = 0;

    public static void main(String[] args) {
        var demo = new ListDemo2();
        demo.add("item 1");
        demo.add(100);
    }

    public void add(Object item) {
        items[count++] = item;
    }
}
