package module9;

public class StringDemo {
    public static void main(String[] args) {
        StringDemo demo = new StringDemo();
        //demo.concat();
        //demo.substring();
        //demo.contain();
        demo.replace('v', 'w');
        demo.replace('J', 'B');
    }

    public void replace(char find, char replace) {
        String str = "this is Java SE Training at Am Metlife Insurance";
        String result = str.replace(find, replace);
        System.out.println("Result = " + result);
    }

    public void contain() {
        String str = "this is Java SE Training at Am Metlife Insurance";
        if (str.contains("Python")) {
            System.out.println("Yes");
        } else {
            System.out.println("Not found");
        }
    }

    public void substring() {
        String str = "my name is John Doe and John Smith is his friend";
        String name = str.substring(11);
        System.out.println("Name = " + name);
        String keyword = str.substring(3, 7);
        System.out.println("Keyword = " + keyword);

        int index = str.indexOf("John"); // return location of the occurrence of the keyword
        System.out.println("index = " + index);
    }

    public void concat() {
        String name = "my name is Azman";
        System.out.println(name.toLowerCase()); // ctl + d = duplicate line
        System.out.println(name.toUpperCase());
        String name2 = new String("Hello");
        String name3 = name + name2; // + = concat
        String name4 = name.concat(name2);
        System.out.printf("Name 3 = %s Name4 = %s \n", name3, name4);
    }
}
