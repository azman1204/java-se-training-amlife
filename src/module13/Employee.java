package module13;
//import java.io.Serializable;
public class Employee implements java.io.Serializable {
    private String name;
    private int age;

    public Employee() {} // no-args constructor

    public void setName(String name) { // setter
        this.name = name;
    }

    public String getName() { // getter
        return name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }
}
