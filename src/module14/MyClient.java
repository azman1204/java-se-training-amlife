package module14;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class MyClient {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 6666);
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            out.writeUTF("Hello...");
            out.flush();
            out.close();
        } catch (IOException e) {
            System.out.println("Got Error " + e.getMessage());
        }
    }
}
