package module14;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServer {
    public static void main(String[] args) {
        try {
            ServerSocket server = new ServerSocket(6666);
            Socket socket = server.accept();
            DataInputStream input = new DataInputStream(socket.getInputStream());
            String message = input.readUTF();
            System.out.println("Message = " + message);
            input.close();
        } catch (IOException e) {
            System.out.println("Gor Error " + e.getMessage());
        }
    }
}
