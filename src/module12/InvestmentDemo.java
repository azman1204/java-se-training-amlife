package module12;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class InvestmentDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter your name : ");
        String name = scanner.next();
        System.out.print("Please enter investment amount : ");
        float investAmount = scanner.nextFloat();
        System.out.print("Please enter number of years to invest : ");
        int yearInvestment = scanner.nextInt();
        float sum = investAmount;
        for (int i=1; i<= yearInvestment; i++) {
            sum = sum + sum * 0.06F;
        }
        String fileName = name + ".txt";
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            writer.write("Name : " + name + "\n");
            writer.write("Initial Investment : " + investAmount + "\n");
            writer.write("Profit : " + sum + "\n");
            writer.close();
        } catch (IOException e) { throw new RuntimeException(e);}
    }
}
