package module12;

import java.io.*;

public class FileDemo {
    public static void main(String[] args) {
        FileDemo demo = new FileDemo();
        //demo.readFile();
        //demo.writeToFile();
        //demo.createFile(10);
        //demo.delete("file-1.txt");
        demo.delete(".txt"); // delete all .txt files
        demo.listFiles();
    }

    // delete file
    public void delete(String fileName) {
        File f = new File("C:\\users\\azman\\desktop\\javatraining");
        File[] files = f.listFiles();
        for(File file : files) {
            String fname = file.getName();
            if (fname.contains(fileName)) {
                file.delete();
            }
        }
    }

    // create files
    public void createFile(int numberOfFile) {
        for (int i=1; i <= numberOfFile; i++) {
            String name = "file-" + i + ".txt";
            File f = new File("C:\\users\\azman\\desktop\\javatraining\\" + name);
            try {
                if (f.createNewFile()) {
                    System.out.println(name + " successfully created");
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    // list all files and directory
    public void listFiles() {
        File file = new File("C:\\users\\azman\\desktop\\javatraining");
        /*String[] contents = file.list();
        for (String content: contents) {
            System.out.println(content);
        }*/

        File[] files = file.listFiles();
        for (File f: files) {
            System.out.println(f.getName() + "\t" + f.length()); // length() - return size of a file in bytes
        }
    }

    public void writeToFile() {
        try {
            //BufferedWriter writer = new BufferedWriter(new FileWriter("quote_of_the_day.txt"));
            FileWriter fileWriter = new FileWriter("quote_of_the_day.txt", true);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            writer.write("Smile all day long...\n");
            writer.write("Happiness is the journey..not the destination\n");
            writer.close();
        } catch (IOException ex) {
            System.out.println("Some error happened");
        }
    }
    // gitlab.com/azman1204/
    public void readFile() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("sample.txt"));
            String str = "";
            while ((str = reader.readLine()) != null) {
                System.out.println(str);
            }
        } catch (IOException e) {
            //throw new RuntimeException(e);
            System.out.println("some error");
        }
    }
}
