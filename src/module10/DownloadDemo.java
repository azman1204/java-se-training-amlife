package module10;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
public class DownloadDemo {
    public static void main(String[] args) {
        try {
            String fileName = "java1.png";
            String website = "http://jomdemy.com/images/"+fileName;
            System.out.println("Downloading image from " + website);
            URL url = new URL(website);
            InputStream input = url.openStream();
            OutputStream output = new FileOutputStream(fileName);
            byte[] buffer = new byte[2048];// 2Kb
            int length = 0;
            while((length = input.read(buffer)) != -1) {
                output.write(buffer, 0, length);
            }
            input.close();
            output.close();
        } catch (Exception ex) {
            System.out.println("Got error " + ex.getMessage());
        }
    }
}
