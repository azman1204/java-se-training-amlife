package module10;
import ij.IJ;
import ij.ImagePlus;
import ij.io.FileSaver;
import ij.process.ImageProcessor;
import java.awt.*;
public class ImageDemo {
    public static void main(String[] args) {
        ImageDemo demo = new ImageDemo();
        demo.generateImage();
    }
    public void generateImage() {
        ImagePlus imp = IJ.openImage("test.jpg");
        ImageProcessor ip = imp.getProcessor();
        ip.setColor(Color.red);
        ip.setLineWidth(4);
        ip.drawRect(50, 50, imp.getWidth() - 100, imp.getHeight() - 100);
        FileSaver fs = new FileSaver(imp);
        fs.saveAsJpeg("test2.jpeg");
    }
}
