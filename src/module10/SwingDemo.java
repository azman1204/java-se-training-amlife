package module10;
import javax.swing.*;
public class SwingDemo extends JPanel {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(200, 200);
        frame.setLayout(null);
        frame.setVisible(true);

        JButton btn = new JButton("Click Me");
        btn.setBounds(100, 100, 100, 40);
        frame.add(btn);
    }
}
