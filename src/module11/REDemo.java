package module11;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class REDemo {
    public static void main(String[] args) {
        REDemo demo = new REDemo();
        //demo.example();
        demo.validateICNo();
    }
    public void validateICNo() {
        String ic = "123456-09-1234";
        Pattern pattern = Pattern.compile("^[0-9]{6}-[0-9]{2}-[0-9]{4}$");
        Matcher matcher = pattern.matcher(ic);
        if (matcher.find()) {
            System.out.println("valid");
        } else {
            System.out.println("not valid");
        }
        // ic pattern must be:
        // 1. must have -2digit- // 2. start with 6 digit // 3. end with 4 digit
    }

    public void example() {
        Pattern pattern = Pattern.compile("home", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher("visit my school");
        if (matcher.find()) {
            System.out.println("yes, there is school keyword inside");
        } else {
            System.out.println("keyword not found");
        }
    }
}
