package module11;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebScrapping {
    public static void main(String[] args) {
        StringBuilder content = new StringBuilder();
        try {
            URL url = new URL("https://msn.com/en-my");
            URLConnection conn = url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = "";
            while ((line = reader.readLine()) != null) {
                content.append(line);
            }

            System.out.println(content);
            Pattern pattern = Pattern.compile("qatar", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(content);
            if (matcher.find()) {
                System.out.println("Yes, there qatar keyword there");
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter("msn.html"));
            writer.write(content.toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
