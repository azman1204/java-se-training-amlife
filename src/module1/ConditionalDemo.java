package module1;

public class ConditionalDemo {
    public static void main(String[] args) {
        int age = 18;
        String level = "";
        // && = AND || = OR
        if (age < 18 && age > 7) {
            level = "teenager";
        } else if (age >= 18 && age < 50) {
            level = "adult";
        } else if (age > 50) {
            level = "old";
        }

        System.out.println(" Level = " + level);

        String name = "X";
        switch (name) {
            case "M":
                System.out.println("Mother");
                break;
            case "F":
                System.out.println("Father");
                break;
            default:
                System.out.println("Others");
        }
    }
}
