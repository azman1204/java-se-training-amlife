package module1;

public class LoopDemo {
    public static void main(String[] args) {
        // while
        int num = 5;
        while (num < 5) {
            System.out.println(num);
            num++; // num = num + 1
        }

        // do .. while
        short age = 5;
        do {
            System.out.println("age = " + age);
            age++;
        } while (age < 5);

        // for
        for(int i=0; i < 10; i++) {
            System.out.println("i = " + i);
        }

        // for each - used with array / collection
        String[] names = {"azman", "CK", "Hafiz", "Ravi", "Sandy", "Jason"};
        for(String name : names) {
            System.out.println(name);
        }
    }
}
