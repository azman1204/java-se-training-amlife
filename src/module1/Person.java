package module1;

public class Person {
    // property / attribute
    String name;
    byte age;
    char gender = 'M';
    boolean isMarried = true;

    // methods / action
    public void run() {
        System.out.println(name + " is running");
    }

    public void eat() {

    }
}
