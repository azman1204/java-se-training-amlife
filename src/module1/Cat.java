package module1;
// this is non-primitive type
public class Cat {
    // class have 2 members
    // 1. property
    // 2. method

    // property
    public String name = "Kitty";
    public String color = "white";


    //methods = action
    public void showDetail() {
        System.out.println("Name = " + name);
    }

    public void run() {
        System.out.println("cat run");
    }

    private void eat() {

    }
}
