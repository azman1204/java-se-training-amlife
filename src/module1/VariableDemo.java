package module1;
// this is single line comment
/*
this is multiple line comments
Variable Type in java:
1. primitive
2. non-primitive
 */
public class VariableDemo {
    public static void main(String[] args) {
        // primitive data type. notice all start small letter
        // whole number
        byte age = 40; // -127 to 127
        short salary = 5000; // -32k to 32k
        int distance = 100000; // -2b to 2b
        long moreDistance = 10000; // trillion

        // decimal point number
        float wage = 5000.50F;
        double distance2 = 50000.4552;

        char letter = 'C';
        boolean yesno = true; // true or false

        // non-primitive data type
        String name = "azman";
        int sum = age + 20; // auto convert byte to integer
        System.out.println("sum of age = " + sum);

        // "new" take class into object
        // one class can create multiple object
        Cat cat = new Cat();
        System.out.println("Cat name = " + cat.name);

        Cat cat2 = new Cat();
        Person p1 = new Person();
        Person p2 = new Person();
        p1.name = "azman";
        p1.age = 40;
        p1.run();

        p2.name = "Ravi";
        p2.age = 25;
    }
}
