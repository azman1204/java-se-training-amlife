package module1;
import java.util.Scanner;

public class ArithmeticDemo {
    public static void main(String[] args) {
        int a = 10 + 2;
        int b = 20 - 10;
        int c = 10 / 2;
        int d = 10 % 3; // remainder / balance
        //printf = print format. \n = new line
        System.out.printf("a = %d, b = %d, c = %d, d = %d \n", a, b, c, d);
        double rand = Math.random();
        int no = (int)(rand * 100); // (int) = casting, convert from a format to other format. double to int
        System.out.println(no);
        // JAVA API / library

        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter a number:");
        int num = scanner.nextInt();
        System.out.println("num = " + num);
    }
}
