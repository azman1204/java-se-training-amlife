package module1;
import java.util.Scanner;

public class GuessGame {
    public static void main(String[] args) {
        int num = (int) (Math.random() * 100) + 1; // between 1 and 100
        int numOfGuess = 0;
        while (true) {
            numOfGuess++;
            System.out.println("Enter a number between 1 and 100 : ");
            Scanner scanner = new Scanner(System.in);
            int playerGuess = scanner.nextInt();

            if (num == playerGuess) {
                System.out.println("Correct!, you win after " + numOfGuess + " guess");
                break; // means exit the loop
            } else {
                if (playerGuess > num) {
                    System.out.println("Guess lower number");
                } else {
                    System.out.println("Guess greater number");
                }
                //System.out.println("Wrong");
            }
        }
    }
}
