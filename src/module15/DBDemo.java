package module15;
import java.sql.*;
public class DBDemo {
    public static void main(String[] args) {
        DBDemo demo = new DBDemo();
        //demo.insert();
        //demo.update();
        demo.delete(3);
        demo.retrieve();
    }

    public Statement getConnection() {
        String path = "C:\\Users\\azman\\Desktop\\javatraining\\sqlite-tools-win32-x86-3400000\\sqlite-tools-win32-x86-3400000\\mydb.db";
        Statement stmt = null;
        try {
            Connection conn = DriverManager.getConnection("jdbc:sqlite:" + path);
            stmt = conn.createStatement();
        } catch (SQLException e) {
            System.out.println("Got error " + e.getMessage());
        }
        return stmt;
    }

    public void delete(int id) {
        String sql = "DELETE FROM employee WHERE id = " + id;
        Statement stmt = this.getConnection();
        try {
            stmt.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void update() {
        String sql = "UPDATE employee SET name = 'Jane Smith' WHERE id = 1";
        Statement stmt = this.getConnection();
        try {
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            System.out.println("Got error " + e.getMessage());
        }
    }

    public void retrieve() {
        try {
            Statement stmt = this.getConnection();
            ResultSet rs = stmt.executeQuery("SELECT * FROM employee");
            while (rs.next()) {
                System.out.println("Name = " + rs.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println("Got error " + e.getMessage());
        }
    }

    public void insert() {
        String sql = "INSERT INTO employee VALUES(3, 'Ronaldo', 36)";
        try {
            Statement stmt = this.getConnection();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                System.out.println("Name = " + rs.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println("Got error " + e.getMessage());
        }
    }
}
