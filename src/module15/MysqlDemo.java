package module15;
import java.sql.*;
public class MysqlDemo {
    public static void main(String[] args) {
        MysqlDemo demo = new MysqlDemo();
        demo.procedure();
        demo.showEmplyee();
    }

    public void procedure() {
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://167.99.79.74:3306/javase", "java", "java@2022");
            String sql = "{ call insert_employee(?, ?, ?) }";
            CallableStatement stmt = conn.prepareCall(sql);
            stmt.setInt(1, 50);
            stmt.setString(2, "Ronaldo");
            stmt.setString(3, "ronaldo@gmail.com");
            stmt.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void showEmplyee() {
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://167.99.79.74:3306/javase", "java", "java@2022");
            System.out.println("Successfully connected !");
            String sql = "SELECT * FROM employee";
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                String name = rs.getString("name");
                String email = rs.getString("email");
                int age = rs.getInt("age");
                System.out.printf("%s \t %s \t %d \n", name, email, age);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
