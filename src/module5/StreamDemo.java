package module5;
import java.util.Arrays;
import java.util.stream.Stream;
public class StreamDemo {
    public static void main(String[] args) {
        StreamDemo demo = new StreamDemo();
        //demo.filter();
        //demo.map();
        //demo.sort();
        //demo.reduce();
        demo.combine();
    }

    public void combine() {
        int[] numbers = {10,2,5,3,5,6,8,7,9,4};
        Arrays.stream(numbers)
        .filter(i -> { return i > 5; })
        .map(i -> { return i * 2; })
        .sorted()
        .forEach(System.out::println);
    }

    public void reduce() {
        int[] numbers = {10,2,5,3,5,6,8,7,9,4};
        int sum = Arrays.stream(numbers).reduce(0, (a, b) -> a + b);
        System.out.println("SUM = " + sum);
    }

    public void sort() {
        Stream.of(10,2,5,3,5,6,8,7,9,4)
                .sorted()
                .forEach(System.out::println);
    }

    public void map() {
        Stream.of(1,2,3,4,5,6,7,8,9,10)
                .map(i -> { return i * 3; })
                .forEach(System.out::println);
    }

    public void filter() {
        // chaining
        Stream.of(1,2,3,4,5,6,7,8,9,10)
                .filter(i -> { return i > 3; })
                .forEach(System.out::println);
    }
}
