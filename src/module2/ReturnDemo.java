package module2;

public class ReturnDemo {
    // void = not return anything
    public static void main(String[] args) {
        ReturnDemo demo = new ReturnDemo();
        int result = demo.sum(10, 5);
        System.out.println("result = " + result);
        String greeting = ReturnDemo.greet("John Doe");
        System.out.println(greeting);
    }

    // static means belong to class, not to object
    public static String greet(String name) {
        return "Hello " + name;
    }

    // this method return "int" value
    private int sum(int num1, int num2) {
        //return "Hello"; // error because it return string
        return num1 + num2;
    }
}
