package module2;
import module1.Cat;
public class EnumDemo {
    // access modifier - public, private, protected
    // apply to class, methods and property
    // public - everybody can access
    // private - only accessible within a class
    // protected - only accessible with the same package
    public static void main(String[] args) {
        EnumDemo demo = new EnumDemo();
        demo.calculate(); // this is accessible within a class
        Cat cat = new Cat();
        //cat.eat(); // this is error
        enum Level {
            LOW, MEDIUM, HIGH
        }

        // the purpose of enum
        // 1. code clarity
        // 2. constant variable / unchangeable variable
        Level myLevel = Level.LOW;
        if (myLevel == Level.LOW) {
            System.out.println("You are playing beginner level");
        }

        enum DayWeek {
            SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
        }

        DayWeek day = DayWeek.MONDAY;
        if (day == DayWeek.SATURDAY || day == DayWeek.SUNDAY) {
            System.out.println("It's week end");
        } else {
            System.out.println("it's working day");
        }

        // enum vs final (constant)
        final int AGE = 10;
        //AGE = 5; // error, can't change constant value
    }

    private void calculate() {

    }
}
