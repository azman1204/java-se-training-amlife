package module4;
// this is a child class
// child inherit all public / protected
// parent property and methods
public class Fox extends Dog {
    String name;
    public static void main(String[] args) {
        Fox fox = new Fox("Foxy");
        fox.printInfo();
    }

    // method overriding
    public void printInfo() {
        System.out.println(" Fox Name " + this.name);
    }

    // constructor - a method,
    // no return, name same as class name
    // automatically called when create new obj
    public Fox(String name) {
        this.name = name;
    }

    public Fox() {}
    public Fox(String name, String color) {}
}
