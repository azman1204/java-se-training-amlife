package module4;
// lambda = a class with only one method
public class LambdaDemo {
    public static void main(String[] args) {
        Message msg = () -> {
            System.out.println("Hello Lambda");
        };
        msg.print();

        Alert alert = (str) -> {
            String s = "You are in " + str;
            return s;
        };
        System.out.println(alert.print("DANGER"));
    }
}

interface Message {
    public void print();
}

interface Alert {
    public String print(String status);
}