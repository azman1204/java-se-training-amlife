package module4;
// interface is like class, but only have declaration but no implementation
public interface Animal {
    public void printInfo(); // method declaration
}
