package module6;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

// the purpose of serialization is to save an object state (data)
public class SerializeDemo {
    public static void main(String[] args) {
        Employee emp = new Employee(); // instantiate an obj
        emp.name = "John Doe";
        emp.address = "London";
        emp.SSN = 123456;
        emp.number = 10000;
        try {
            FileOutputStream out = new FileOutputStream("employee.ser");
            ObjectOutputStream out2 = new ObjectOutputStream(out);
            out2.writeObject(emp); // write object to a file
            out2.close();
            out.close();
            System.out.println("Serialize to a file completed");
        } catch (IOException ex) {
            System.out.println("Got error " +ex.getMessage());
        }
    }
}

