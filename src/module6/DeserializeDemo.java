package module6;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
public class DeserializeDemo {
    public static void main(String[] args) {
        try {
            FileInputStream in = new FileInputStream("employee.ser");
            ObjectInputStream in2 = new ObjectInputStream(in);
            Employee emp = (Employee) in2.readObject();
            System.out.println("Employee Name = " + emp.name);
        } catch (IOException ex) {
            System.out.println("Got error " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("Got error " + ex.getMessage());
        }
    }
}
