package module7;
// child class
public class Car extends Vehicle {
    @Override
    public void printInfo() {
        super.printInfo();
    }

    @Deprecated
    // deprecated means will be dropped in near future
    public void changeGear() {

    }

    public static void main(String[] args) {
        Car car = new Car();
        car.changeGear();
    }
}
